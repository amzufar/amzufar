from django.shortcuts import render


def home(request):
    return render(request, 'main/home.html')
def education(request):
    return render(request, 'main/education.html')
def contacts(request):
    return render(request, 'main/contacts.html')
