from django.urls import path

from . import views

app_name = 'main'

urlpatterns = [
    path('', views.home, name='home'),
    path('education', views.education, name='education'),
    path('contacts', views.contacts, name='contacts')
]
